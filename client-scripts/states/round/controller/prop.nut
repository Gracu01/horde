addEvent("prop:onVisual")

local factor = 200.0

class PropController extends PlayerController {
    synchronization = null

    _lower_height_limit = 0
    _upper_height_limit = 0
    _camera_vob = null
    _selector_vob = null
    _attached_vob = null
    _lock_rotation = false
    _ui = null

    constructor(available_vobs) {
        this._camera_vob = ArcballCamera()
        this._selector_vob = VobSelector(available_vobs)
        local vob_data = this._selector_vob.selected()
        
        this._attached_vob = VobFollower(heroId, vob_data.visual)
        this._attached_vob.spawn()
        
        this._lower_height_limit = vob_data.lower_height
        this._upper_height_limit = vob_data.upper_height
        this._correctHeight()

        this.synchronization = PropSynchronization(this._selector_vob.index, this._attached_vob.height_offset)

        this._ui = Draw(0, 0, "Controls:\n<-/-> - Change visual.\nZ/X - Change height.\nR - Lock rotation.")
        this._ui.setPosition(8196 - this._ui.width - 50, 6000)
    }

    function _correctHeight() {
        if (this._attached_vob.height_offset < this._lower_height_limit) {
            this._attached_vob.height_offset = this._lower_height_limit
        } else if (this._attached_vob.height_offset > this._upper_height_limit) {
            this._attached_vob.height_offset = this._upper_height_limit
        }
    }

    function _updateHeight(delta_ms) {
        if (!chatInputIsOpen()) {
            if (isKeyPressed(KEY_Z)) {
                this._attached_vob.height_offset += delta_ms * 0.1
                this._correctHeight()

                callEvent("prop:onVisual", this._selector_vob.index, this._attached_vob.height_offset)
            } else if (isKeyPressed(KEY_X)) {
                this._attached_vob.height_offset -= delta_ms * 0.1
                this._correctHeight()

                callEvent("prop:onVisual", this._selector_vob.index, this._attached_vob.height_offset)
            }
        }
    }

    function init() {
        this._ui.visible = true
        Camera.setTargetVob(this._attached_vob.vob)
    }

    function dispose() {
        this._ui.visible = false

        Camera.setTargetPlayer(heroId)
        Camera.movementEnabled = true

        this._attached_vob.clear()
    }

    function update(delta_ms) {
        this._attached_vob.update()
        this.synchronization.update()

        this._updateHeight(delta_ms)

        if (this._lock_rotation) {
            this._camera_vob.update(this._attached_vob.vob.getPosition())
        }
    }

    function onInput(key) {
        if (this._selector_vob.handleInput(key)) {
            local vob_data = this._selector_vob.selected()
            this._lower_height_limit = vob_data.lower_height
            this._upper_height_limit = vob_data.upper_height

            // Correct height offset after changing vob
            this._correctHeight()
            this._attached_vob.setVisual(vob_data.visual)

            setPlayerScale(heroId, vob_data.scale, 1, vob_data.scale)
            callEvent("prop:onVisual", this._selector_vob.index, this._attached_vob.height_offset)
        }

        if (key == KEY_R) {
            Camera.movementEnabled = this._lock_rotation
            this._lock_rotation = !this._lock_rotation

            if (this._lock_rotation) {
                this._camera_vob.activate(this._attached_vob.vob.getPosition())
            }
        }
    }

    function onMouseMove(x, y) {
        if (this._lock_rotation) {
            x = x * factor * WorldTimer.frameTimeSecs
            y = y * factor * WorldTimer.frameTimeSecs

            this._camera_vob.move(x, y)
        }
    }

    function onMouseWheel(value) {
        if (this._lock_rotation) {
            this._camera_vob.zoom -= value * 10.0
            if (this._camera_vob.zoom < PROP_ZOOM_MIN) {
                this._camera_vob.zoom = PROP_ZOOM_MIN
            } else if (this._camera_vob.zoom > PROP_ZOOM_MAX) {
                this._camera_vob.zoom = PROP_ZOOM_MAX
            }
        }
    }

    function onAnim(id) {

    }

    function onFocusCollect(focuses) {

    }
}