class TauntsManager {
    _players = null

    constructor() {
        this._players = {}
    }

    function play(pid, filename) {
        if (!(pid in this._players)) {
            // Allocate entry
            this._players[pid] <- null
        }

        local sound = this._players[pid]
        if (!sound) {
            // Allocate sound
            sound = Sound3d(filename)
            sound.setTargetPlayer(pid)

            this._players[pid] = sound
        }
        
        if (!sound.isPlaying()) {
            sound.file = filename
            sound.play()
        }
    }

    function stop(pid) {
        if (pid in this._players) {
            this._players[pid] = null
        }
    }

    function clear() {
        foreach (sfx in this._players) {
            if (sfx) {
                sfx.stop()
            }
        }

        this._players.clear()
    }
}