class VobFollower {
    idx = -1
    height_offset = 0
    vob = null

    constructor(idx, visual) {
        this.idx = idx
        this.vob = Vob(visual)
    }

    function setVisual(name) {
        this.vob.visual = name
    }

    function clear() {
        // Make sure that vob is deleted
        this.vob.removeFromWorld()
        this.vob = null
    }

    function spawn() {
        this.vob.addToWorld()
    }

    function unspawn() {
        this.vob.removeFromWorld()
    }

    function update() {
        local position = getPlayerPosition(this.idx)
        local angle = getPlayerAngle(this.idx)

        if (isPlayerStreamed(this.idx)) {
            setPlayerVisualAlpha(this.idx, 0.0)

            this.vob.beginMovement()
            this.vob.setPosition(position.x, position.y + this.height_offset, position.z)
            this.vob.setRotation(0, angle, 0)
            this.vob.endMovement()
        }
    }
}

class VobFollowerManager {
    _players = null
    _followers = null
    _spawning = false

    constructor() {
        this._players = {}
        this._followers = []
    }

    function getById(pid) {
        return pid in this._players ? this._players[pid] : null
    }

    function getByPtr(ptr) {
        foreach (follower in this._followers) {
            if (follower.vob.ptr == ptr) {
                return follower
            }
        }

        return null
    }

    function insert(pid, visual) {
        local follower = VobFollower(pid, visual)

        this._players[pid] <- follower
        this._followers.push(follower)

        if (this._spawning) {
            follower.spawn()
        }
    }

    function remove(pid) {
        if (pid in this._players) {
            this._players[pid] = null

            foreach (i, follower in this._followers) {
                if (follower.idx == pid) {
                    

                    this._followers[i] = this._followers[this._followers.len() - 1]
                    this._followers.pop()
                    return
                }
            }
        }
    }

    function clear() {
        foreach (follower in this._followers) {
            follower.clear()
        }

        this._followers.clear()
        this._players.clear()
    }

    function spawn() {
        this._spawning = true

        foreach (follower in this._followers) {
            follower.spawn()
        }
    }

    function unspawn() {
        this._spawning = false

        foreach (follower in this._followers) {
            follower.unspawn()
        }
    }

    function update() {
        foreach (follower in this._followers) {
            follower.update()
        }
    }
}