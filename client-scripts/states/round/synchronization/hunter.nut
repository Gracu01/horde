class HunterSynchronization {
    function invalidHit() {
        local packet = InvalidHitMessage().serialize()
        packet.send(RELIABLE)
    }
}