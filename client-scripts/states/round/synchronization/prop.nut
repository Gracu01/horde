class PropSynchronization {
    current_vob_index = -1
    current_vob_height = 0

    _last_vob_index = -1
    _last_vob_height = 0
    _last_vob_update_ts = -1

    constructor(index, height) {
        this.current_vob_index = index
        this.current_vob_height = height
        this._last_vob_index = index
        this._last_vob_height = height
        this._last_vob_update_ts = WorldTimer.totalTime
    }

    function _broadcast() {
        local message = PropUpdateClientMessage()
        local need_update = false

        if (this._last_vob_index != this.current_vob_index) {
            message.vob_index = this.current_vob_index
            this._last_vob_index =this. current_vob_index
            need_update = true
        }

        if (this._last_vob_height != this.current_vob_height) {
            message.vob_height = this.current_vob_height
            this._last_vob_height = this.current_vob_height
            need_update = true
        }

        if (need_update) {
            message.serialize().send(RELIABLE)
        }
    }

    function update() {
        // Rate-limit vob update
        local delta_ms = WorldTimer.totalTime - this._last_vob_update_ts
        if (delta_ms > 500) {
            this._broadcast()
            this._last_vob_update_ts = WorldTimer.totalTime
        }
    }
}
