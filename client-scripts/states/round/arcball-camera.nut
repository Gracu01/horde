local function deg2rad(deg) {
    return deg * 0.0174532925
}

class ArcballCamera {
    yaw = 30
    pitch = 0
    zoom = 0

    function activate(point) {
        local position = Camera.getPosition()
        local rotation = Camera.getRotation()

        this.yaw = -(rotation.y - 60) + 210
        this.pitch = rotation.x
        this.zoom = getDistance3d(position.x, position.y, position.z, point.x, point.y, point.z)
    }

    function move(x, y) {
        yaw += x
        pitch += y

        if (pitch > 89.0) {
            pitch = 89.0
        } else if (pitch < -89.0) {
            pitch = -89.0
        }
    }

    function update(point) {
        local direction = Vec3(0, 0, 0)
        direction.x = cos(deg2rad(this.yaw)) * cos(deg2rad(this.pitch))
        direction.y = sin(deg2rad(this.pitch))
        direction.z = sin(deg2rad(this.yaw)) * cos(deg2rad(this.pitch))
        direction *= this.zoom

        local y = -(this.yaw - 60) + 210
        Camera.setRotation(this.pitch, y, 0)
        Camera.setPosition(point.x + direction.x, point.y + direction.y, point.z + direction.z)
    }
}