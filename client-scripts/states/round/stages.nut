class RoundStage extends BState {
    function onUpdate(delta_ms, time_left_ms) {

    }
}

class HidingStage extends RoundStage {
    _infos = null
    _props = null
    _player_type = null

    constructor(player_type, infos, props) {
        this._infos = infos
        this._props = props
        this._player_type = player_type
    }

    function onEnter() {
        if (this._player_type == PlayerType.HUNTER) {
            this._infos.push(["You are hunter!"], 2000)
            this._infos.push(["Wait for players to hide,", " before you go hunt them down."], 4000)
            this._infos.push(["Use mouse cursor to hover over prop", "and right mouse button to select it."], 5000)
            this._infos.push(["Then attack them to deal damage."], 3000)
            this._infos.push(["Be careful! Hitting wrong prop,", "will take small amount of your health."], 5000)
        } else if (this._player_type == PlayerType.PROP) {
            this._infos.push(["You are prop!"], 2000)
            this._infos.push(["Change your visual and hide before time runs out."], 3000)
        }

        if (this._player_type != PlayerType.HUNTER) {
            this._props.spawn()
        }
    }

    function onExit() {
        
    }
}

class HuntingStage extends RoundStage {
    _infos = null
    _props = null
    _player_type = null
    _taunts = null

    constructor(player_type, infos, props) {
        this._infos = infos
        this._props = props
        this._player_type = player_type
        this._taunts = TauntsManager()
    }

    function onEnter() {
        if (this._player_type == PlayerType.HUNTER) {
            this._infos.push(["Now go! Find as many props as you can."], 2000)
        } else if (this._player_type == PlayerType.PROP) {
            this._infos.push(["Hunters are here!"], 2000)
            this._infos.push(["Now the real game of hide and seek begins."], 3000)
        } else if (this._player_type == PlayerType.SPECTATOR) {
            this._infos.push(["You are spectator!"], 3000)
            this._infos.push(["Wait for round to finish."], 3500)
        }

        if (this._player_type == PlayerType.HUNTER) {
            this._props.spawn()
        }
    }

    function onExit() {
        this._taunts.clear()
        this._props.unspawn()
    }

    </ packet = TauntPlayServerMessage />
    function onPlayerTauntPlay(data) {
        if (isPlayerStreamed(data.idx)) {
            local sound_name = taunt_registry.getById(data.sound_index)
            this._taunts.play(data.idx, sound_name)
        }
    }

    </ event = "onPlayerUnspawn" />
    function onPlayerUnspawn(pid) {
        this._taunts.stop(pid)
    }

    </ event = "hunter:onAttack" />
    function onHunterAttack(follower) {
        if (this._player_type == PlayerType.HUNTER) {
            if (follower) {
                hitPlayer(heroId, follower.idx)
            } else {
                HunterSynchronization.invalidHit()
            }
        }
    }
}