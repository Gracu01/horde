class BoardUI extends Texture {
	_labels = null

	constructor(lines_count) {
		base.constructor(0, 0, anx(250), 200 * (lines_count + 1), "MENU_INGAME.TGA")

		this._labels = array(lines_count)
		for (local i = 0; i < lines_count; ++i) {
			this._labels[i] = Draw(20, 200 * (i + 1), "")
		}
	}

    function setPosition(x, y) {
        base.setPosition(x, y)

        foreach (i, label in this._labels) {
            label.setPosition(x + 100, y + 200 * i + 100)
        }
    }

    function setPositionPx(x, y) {
        base.setPositionPx(x, y)

        foreach (i, label in this._labels) {
            label.setPositionPx(x + anx(100), y + any(200) * i + any(100))
        }
    }

    function setVisible(toggle) {
        this.visible = toggle

        foreach (i, label in this._labels) {
            label.visible = toggle
        }
    }

	function updateLine(nr, text) {
		this._labels[nr].text = text
	}

    function updateColor(nr, r, g, b) {
        this._labels[nr].setColor(r, g, b)
    }

	function clear() {
		foreach (label in this._labels) {
			label.text = ""
		}
	}
}

class BoardLobbyUI extends BoardUI {
    constructor() {
        base.constructor(7)
        this.reset()
    }

    function reset() {
        this.clear()

        this.updateLine(0, "Game starts in:")
        this.updateColor(0, 255, 150, 0)

        this.updateLine(3, "Next map:")
        this.updateColor(3, 255, 220, 0)

        this.updatePlayers(0)
    }

    function updateTimer(seconds) {
        if (seconds > 0) {
            this.updateLine(0, "Game starts in:")
            this.updateColor(0, 255, 150, 0)
            this.updateColor(1, 255, 255, 255)
            this.updateLine(1, format("%02d:%02d:%02d", seconds / 3600, (seconds / 60) % 60, seconds % 60))
        } else {
            this.updateColor(0, 0, 255, 0)
            this.updateLine(0, "Game is starting...")
            this.updateLine(1, "")
        }
    }

    function updateMap(name) {
        this.updateColor(4, 200, 200, 200)
        this.updateLine(4, name)
    }

    function updatePlayers(count) {
        this.updateColor(6, 230, 230, 230)
        this.updateLine(6, format("Players: %d", count))
    }
}

class BoardRoundUI extends BoardUI {
    _hunters = null
    _props = null

    constructor() {
        base.constructor(10)

        this._hunters = Draw(0, 0, "")
        this._props = Draw(0, 0, "")

        this.reset()
    }

    function reset() {
        this.clear()

        this.updateLine(0, "Time left:")
        this.updateColor(0, 255, 220, 0)

        this.updateLine(3, "Stage:")
        this.updateColor(3, 255, 220, 0)

        this.updateLine(5, "Class:")
        this.updateColor(5, 255, 220, 0)

        this.updateLine(8, "Hunters:")
        this.updateColor(8, 255, 50, 0)
        this.updateHunters(0)

        this.updateLine(9, "Props:")
        this.updateColor(9, 68, 183, 255)
        this.updateProps(0)
    }

    function updateTimer(seconds) {
        if (seconds >= 0) {
            this.updateColor(1, 255, 255, 255)
            this.updateLine(1, format("%02d:%02d:%02d", seconds / 3600, (seconds / 60) % 60, seconds % 60))
        }
    }

    function setPosition(x, y) {
        base.setPosition(x, y)

        local position = this._labels[8].getPosition()
        this._hunters.setPosition(position.x + this._labels[8].width + 50, position.y)

        local position = this._labels[9].getPosition()
        this._props.setPosition(position.x + this._labels[9].width + 50, position.y)
    }

    function setPositionPx(x, y) {
        base.setPositionPx(x, y)

        local position = this._labels[8].getPosition()
        this._hunters.setPosition(position.x + label.width, position.y)

        local position = this._labels[9].getPosition()
        this._props.setPosition(position.x + label.width, position.y)
    }

    function setVisible(toggle) {
        base.setVisible(toggle)

        this._hunters.visible = toggle
        this._props.visible = toggle
    }

    function updateStage(type) {
        switch (type) {
        case StageType.HIDING:
            this.updateLine(4, "Hiding")
            this.updateColor(4, 235, 235, 235)
            break

        case StageType.HUNTING:
            this.updateLine(4, "Hunting")
            this.updateColor(4, 255, 255, 255)
            break
        }
    }

    function updateClass(type) {
        switch (type) {
        case PlayerType.HUNTER:
            this.updateLine(6, "Hunter")
            this.updateColor(6, 255, 50, 0)
            break

        case PlayerType.PROP:
            this.updateLine(6, "Prop")
            this.updateColor(6, 68, 183, 255)
            break

        case PlayerType.SPECTATOR:
            this.updateLine(6, "Spectator")
            this.updateColor(6, 157, 162, 165)
            break
        }
    }

    function updateHunters(count) {
        this._hunters.text = count.tostring()
    }

    function updateProps(count) {
        this._props.text = count.tostring()
    }
}