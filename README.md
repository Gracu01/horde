# Introduction

**Horde** is a gamemode, which implements the gameplay in the form of battle that involves...

# Rules

- To win round your team has...

# Installation

Include `scripts.xml` to your project, this can be done by adding this line to your `config.xml` file.

Example:

```xml
<import src="gamemodes/horde/scripts.xml" />
```

# License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
