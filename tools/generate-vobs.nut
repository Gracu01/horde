local vobs = [
    "NW_HARBOUR_RINGBIG_01.3DS",
    "NW_HARBOUR_FISHINGNETBIG.3DS",
    "NW_HARBOUR_ROPE_01.3DS",
    "NW_HARBOUR_BARRELGROUP_02.3DS",
    "NW_HARBOUR_BARRELGROUP_01.3DS",
    "NW_HARBOUR_IRONROPEHOLDER_01.3DS",
    "NW_HARBOUR_IRONROPEHOLDER_WROPE_01.3DS",
    "NW_HARBOUR_CRATEPILE_01.3DS",
    "FIREPLACE_NW_LAMP_01.ASC",
    "OC_SAECKE_01.3DS",
    "NW_HARBOUR_ANCHOR_01.3DS",
    "NW_MISC_BOARDS_01B.3DS",
    "NW_HARBOUR_CANON_01.3DS",
    "CAULDRON_OC.ASC",
    "HERB_NW_MISC_01.ASC",
    "HERB_NW_MISC_01.ASC",
    "OC_SAECKE_01.3DS",
    "BENCH_NW_CITY_02.ASC",
    "NW_CITY_TABLE_PEASANT_01.3DS",
    "CHAIR_NW_NORMAL_01.ASC",
    "OC_FIREWOOD_V2.3DS",
    "BEDHIGH_NW_NORMAL_01.ASC",
    "NW_CITY_CUPBOARD_POOR_01.3DS",
    "NW_CITY_CUPBOARD_POOR_02.3DS",
    "OC_SHELF_V3.3DS",
    "NW_CITY_TABLE_NORMAL_01.3DS",
    "CHAIR_NW_NORMAL_01.ASC",
    "NW_HARBOUR_CRATEPILE_01.3DS",
    "THRONE_NW_CITY_01.ASC",
    "BEDHIGH_NW_EDEL_01.ASC",
    "STOVE_NW_CITY_01.ASC",
    "NW_CITY_CUPBOARD_RICH_01.3DS",
    "BEDHIGH_NW_MASTER_01.ASC",
    "NW_CITY_TABLE_PEASANT_01.3DS",
    "DT_FIREPLACE_V1.3DS",
    "OC_LEATHERSTAND_V01.3DS",
    "NW_HARBOUR_BARREL_01.3DS",
    "OC_FIREWOOD_V1.3DS",
    "BSANVIL_OC.MDS",
    "BSCOOL_OC.MDS",
    "BSSHARP_OC.MDS",
    "OC_FIREWOOD_V3.3DS",
    "OC_FIREWOOD_V3.3DS",
    "NW_MISC_HUMAN_STANDY.3DS",
    "BOOK_NW_CITY_CUPBOARD_01.ASC",
    "NW_CITY_BOOKSHELF_01.3DS",
    "NW_CITY_TABLE_PEASANT_01.3DS",
    "OC_MOB_SHELVES_BIG.3DS",
    "NW_CITY_CART_01.3DS",
    "NW_HARBOUR_ROPE_01.3DS",
]

local function vob_lower_height(vob, length) {
    local position = vob.getPosition()
    local direction = Vec3(0, -1, 0) * length

    local result = GameWorld.traceRayNearestHit(position, direction, TRACERAY_VOB_IGNORE | TRACERAY_STAT_POLY | TRACERAY_VOB_IGNORE_NO_CD_DYN)
    if (result) {
        return -((position.y - result.intersect.y) - (position.y - vob.bbox3dWorld.mins.y))
    }

    return PROP_LOWER_HEIGHT
}

/////////////////////////////////////////
///	Events
/////////////////////////////////////////

local function cmd_handler(cmd, params) {
    if (cmd == "generate-vobs") {
        local hero = Vob(getPlayerPtr(heroId))
        local position = hero.getPosition()
        local width = hero.bbox3dWorld.maxs.x - hero.bbox3dWorld.mins.x

        local done = {}
        foreach (name in vobs) {
            if (!(name in done)) {
                local vob = Vob(name)
                vob.setPosition(position.x, position.y, position.z)

                local vob_width = vob.bbox3dWorld.maxs.x - vob.bbox3dWorld.mins.x
                print("{ visual = \"" + name + "\", lower_height = " + vob_lower_height(vob, 300) + ", upper_height = " + PROP_UPPER_HEIGHT + ", scale = " + min(vob_width / width, 1.0) + " },")

                done[name] <- true
            }
        }
    }
}

addEventHandler("onCommand", cmd_handler)
