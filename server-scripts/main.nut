class Server {
    states = null
    players = null

    constructor(max_slots) {
        this.states = BStateMachine()
        this.players = PlayerManager(max_slots)
    }
}

/////////////////////////////////////////
///	Events
/////////////////////////////////////////

local function init_handler() {
    server <- Server(getMaxSlots())
    // Initial state
    server.states.push(LobbyState())
}

addEventHandler ("onInit", init_handler)

local function player_join_handler(pid) {
    server.players.insert(pid)
}

addEventHandler("onPlayerJoin", player_join_handler, 10)

local function player_disconnect_handler(pid, reason) {
    server.players.remove(pid)
}

addEventHandler("onPlayerDisconnect", player_disconnect_handler, 999999)