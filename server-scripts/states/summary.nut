class SummaryState extends BState {
    _timeout_timer_ = -1
    _winner_type = 0

    constructor(winner_type) {
        this._winner_type = winner_type
    }

    function onEnter() {
        this._timeout_timer_ = setTimer(this.onTimeout.bindenv(this), 5000, 0)

        local packet = SummaryMessage(this._winner_type).serialize()
        packet.sendToAll(RELIABLE)
    }

    function onExit() {
        if (this._timeout_timer_ != -1) {
            killTimer(this._timeout_timer_)
            this._timeout_timer_ = -1
        }

        foreach (player in server.players.online) {
            unspawnPlayer(player.id)
        }
    }

    function onTimeout() {
        this.machine.change(LobbyState())
    }

    </ event = "onPlayerHit" />
    function onGotHit(pid, kid, desc) {
        cancelEvent()
    }

    </ event = "onPlayerMessage" />
    function onMessage(pid, text) {
        foreach (player in server.players.online) {
            sendPlayerMessageToPlayer(pid, player.id, 235, 235, 235, text)
        }

        print(getPlayerName(pid) + ": " + text)
    }
}