class LobbyState extends BState {
    _update_timer_ = -1
    _round_start_ts = 0
    _map = 0

    function onEnter() {
        this._update_timer_ = setTimer(this.onUpdate.bindenv(this), 200, 0)
        this._round_start_ts = getTickCount() + LOBBY_TIME_SEC * 1000
        this._map = map_registry.entries[rand() % map_registry.entries.len()]

        local packet = SwitchToLobbyMessage(LOBBY_TIME_SEC * 1000, this._map.index).serialize()
        packet.sendToAll(RELIABLE)
    }

    function onExit() {
        if (this._update_timer_ != -1) {
            killTimer(this._update_timer_)
            this._update_timer_ = -1
        }
    }

    function onUpdate() {
        local current_ts = getTickCount()
        if (this._round_start_ts - current_ts <= 0) {
            if (server.players.online.len() >= 2) {
                this.machine.change(RoundState(this._map))
            } else {
                sendMessageToAll(255, 0, 0, "Not enought players to start!")
                this._round_start_ts = current_ts + LOBBY_TIME_SEC * 1000
                
                local packet = LobbyUpdateMessage(LOBBY_TIME_SEC * 1000).serialize()
                packet.sendToAll(RELIABLE)
            }
        }
    }

    </ event = "onPlayerJoin" />
    function onJoin(pid) {
        sendMessageToAll(0, 255, 0, format("%s joined the lobby.", getPlayerName(pid)))

        local packet = SwitchToLobbyMessage(this._round_start_ts - getTickCount(), this._map.index).serialize()
        packet.send(pid, RELIABLE)
    }

    </ event = "onPlayerDisconnect" />
    function onDisconnect(pid, reason) {
        sendMessageToAll(255, 0, 0, format("%s left the lobby.", getPlayerName(pid)))
    }

    </ event = "onPlayerHit" />
    function onGotHit(pid, kid, desc) {
        cancelEvent()
    }

    </ event = "onPlayerCommand" />
    function onCommand(pid, cmd, params) {
        // TODO: refactor me
        if (cmd == "timeout") {
            if (isPlayerAdmin(pid)) {
                local args = sscanf("d", params)
                if (args) {
                    this._round_start_ts = getTickCount() + args[0] * 1000

                    local message = LobbyUpdateMessage()
                    message.time_left_ms = args[0] * 1000
                    message.serialize().sendToAll(RELIABLE)
                } else {
                    sendMessageToPlayer(pid, 255, 0, 0, "ACP: Type /timeout seconds")
                }
            }
        } else if (cmd == "map") {
            if (isPlayerAdmin(pid)) {
                local args = sscanf("d", params)
                if (args) {
                    this._map = map_registry.entries[args[0]]

                    local message = LobbyUpdateMessage()
                    message.map_index = this._map.index
                    message.serialize().sendToAll(RELIABLE)
                } else {
                    sendMessageToPlayer(pid, 255, 0, 0, "ACP: Type /map index")
                }
            }
        }
    }

    </ event = "onPlayerMessage" />
    function onMessage(pid, text) {
        foreach (player in server.players.online) {
            sendPlayerMessageToPlayer(pid, player.id, 235, 235, 235, text)
        }

        print(getPlayerName(pid) + ": " + text)
    }
}