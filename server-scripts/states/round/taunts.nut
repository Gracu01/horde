class TauntsManager {
    _players = null
    _duration_ms = -1
    _time_left_ms = -1

    constructor(players, seconds) {
        this._players = players
        this._duration_ms = seconds * 1000
        this._time_left_ms = seconds * 1000
    }

    function _collectProps(distance) {
        local props = []
        foreach (prop in this._players.props) {
            local far_enought = true
            foreach (hunter in this._players.hunters) {
                local p1 = getPlayerPosition(prop.id)
                local p2 = getPlayerPosition(hunter.id)
                local delta = getDistance3d(p1.x, p1.y, p1.z, p2.x, p2.y, p2.z)

                if (delta <= distance) {
                    far_enought = false
                }
            }

            if (far_enought) {
                props.push(prop)
            }
        }

        return props
    }

    function update(delta_ms) {
        this._time_left_ms -= delta_ms
        if (this._time_left_ms <= 0) {
            local props = this._collectProps(PROP_TAUNT_MIN_DISTANCE)
            if (props.len() > 0) {
                local count = max(1, ceil(props.len() * 0.4))
                for (local i = 0; i < count; ++i) {
                    local prop = props[i]
                    
                    local streamed = getStreamedPlayersByPlayer(prop.id)
                    streamed.push(prop.id)

                    local sync = RoundSynchronization(streamed)
                    sync.playTaunt(prop, rand() % 5)
                }
            }

            this._time_left_ms = this._duration_ms
        }
    }
}