class RoundUpdateMessage extends BPacketMessage {
    </ type = BPacketInt32, optional = true />
    time_left_ms = null

    </ type = BPacketInt8, optional = true />
    player_type = null

    </ type = BPacketInt8, optional = true />
    stage_type = null

    </ type = BPacketUInt8, optional = true />
    hunters_count = null

    </ type = BPacketUInt8, optional = true />
    props_count = null
}

class InvalidHitMessage extends BPacketMessage {
    
}

class PropCreateMessage extends BPacketMessage {
    </ type = BPacketUInt16 />
    idx = -1

    </ type = BPacketUInt8 />
    vob_index = -1
}

class PropDestroyMessage extends BPacketMessage {
    </ type = BPacketUInt16 />
    idx = -1
}

class PropUpdateClientMessage extends BPacketMessage {
    </ type = BPacketUInt8, optional = true />
    vob_index = null

    </ type = BPacketInt16, optional = true />
    vob_height = null
}

class PropUpdateServerMessage extends BPacketMessage {
    </ type = BPacketUInt16 />
    idx = -1

    </ type = BPacketUInt8, optional = true />
    vob_index = null

    </ type = BPacketInt16, optional = true />
    vob_height = null
}

class TauntPlayClientMessage extends BPacketMessage {
    </ type = BPacketUInt16 />
    sound_index = -1
}

class TauntPlayServerMessage extends BPacketMessage {
    </ type = BPacketUInt16 />
    idx = -1

    </ type = BPacketUInt16 />
    sound_index = -1
}

class SummaryMessage extends BPacketMessage {
    </ type = BPacketUInt8 />
    winner_type = -1
}